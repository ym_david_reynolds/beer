<?php

require 'Beer.php';

class BeerTest extends PHPUnit_Framework_TestCase
{
    public $beerInstance;

    public function setUp()
    {
        $this->beerInstance = new Beer();
    }

    public function testIfBeerWasPassed(){
    	// Suppose we have 100 beers...
    	$this->beerInstance->setBeers(100);

    	// ... And we take one down...
    	$this->beerInstance->takeOneDown();

    	// ... we should now have 99 gumballs remaining in the machine right?
    	$this->assertEquals(99, $this->beerInstance->getBeers()); 
    }
}