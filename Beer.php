<?php

class Beer{

	private $beers;


	// Get the amount of beers still on the wall
	public function getBeers(){
		return $this->beers;
	}

	// Set the amount of beers on the wall
	public function setBeers($amount){
		$this->beers = $amount;
	}

	// The user takes one down and passes it around
	public function takeOneDown(){
		$this->setBeers($this->getBeers() - 1);
	}
}